package com.example.aswin.booksencyclopedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Book> listBooks;
    RecyclerView rvBooks;
    BooksAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listBooks = new ArrayList<>();

        Book book1 = new Book();
        book1.title = "Beginning Android Application Development";
        book1.author = "Wei-Meng Lee";
        book1.price = 40.5;
        book1.thumbnail = R.drawable.book1;

        Book book2 = new Book();
        book2.title = "Software Engineering: A Practitioner's Approach";
        book2.author = "Roger S. Pressman";
        book2.price = 80.9;
        book2.thumbnail = R.drawable.book2;

        listBooks.add(book1);
        listBooks.add(book2);

        adapter = new BooksAdapter(this, listBooks);

        rvBooks = findViewById(R.id.rvBooks);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvBooks.setLayoutManager(linearLayoutManager);
        rvBooks.setAdapter(adapter);
    }
}
